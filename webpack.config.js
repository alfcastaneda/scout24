const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin({
  filename: "styles.css",
});
module.exports = (env = {}) => {
  return {
    context: __dirname,
    entry: [
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true',
      './src/app.module.js'
    ],

    devServer: {
      contentBase: './dist'
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({
        template: 'src/index.html'
      }),
      extractSass
    ],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },
    module: {
      rules: [
      {
        test: /\.scss$/,
        use: env.prod ?
          extractSass.extract({
            use: [
              {loader: "css-loader"},
              {loader: "sass-loader"}
            ],
          }) : [
          {loader: "style-loader"},
          {loader: "css-loader"},
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              data: '@import "variables";',
              includePaths: [
                path.join(__dirname, 'src')
              ]
            }
          }
        ],
      },
      {
        test: /\.js$/,
        use: [{
          loader: 'ng-annotate-loader',
          options: {
            add: false,
            map: false,
          }
        },{
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
          ],
        exclude: /node_modules/,

      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader' ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      },
      {
        test: /\.(json)$/,
        use: ['json-loader']
      },
      {
        test: /\.html$/,
        use: [
          { loader:  'ngtemplate-loader?relativeTo=' + __dirname + '/' },
          { loader: 'html-loader' }
        ],
        exclude: [
          path.resolve(__dirname, 'src/index.html')
        ]
      }
    ]
  }
}};
