import './app.scss';
import templateUrl from './app.html';
import {copy, isDefined} from 'angular';

const AppComponent = {
  templateUrl,
  controller: class AppComponent {
    constructor(HttpService) {
      'ngInject';
      this.HttpService = HttpService;
      this.selectedFeatures = [];
      this.byFeatures = [];
      this.byCommercialization = [];
    }

    $onInit() {
      this.defaultCommercializationType = 'ALL';
      this.HttpService.getRealStates()
        .then((response) => {
          this.title = response.geoName;
          this.baseStates = response.resultlistEntries;
          this.byFeatures = this.byCommercialization = copy(this.baseStates);
          this.updateStates();
          this.availableFeatures = this.getFeatures(response.resultlistEntries);
        })
    }

    getFeatures(list) {
      return list
        .reduce((acc, currentState) => {
          currentState.features.forEach(f => {
            if (!acc.includes(f)) {
              acc.push(f);
            }
          })
          return acc;
        }, [])
    }

    updateStates() {
      this.realStates = this.baseStates.filter(st => {
        return this.byFeatures.find(s => s.id === st.id ) !== undefined && this.byCommercialization.find(s => s.id === st.id) !== undefined;
      });
    }

    filterStatesByFeatures(selected) {
      this.byFeatures  = selected.length ?  this.baseStates.filter(t => selected.every(elem => t.features.indexOf(elem) > -1)) : copy(this.baseStates);
      this.updateStates();
    }

    updateStatesByCommercialization(value) {
      this.byCommercialization = value !== 'ALL' ? this.baseStates.filter(s => s.commercializationType === value) : copy(this.baseStates);
      this.updateStates();
    }
  }
}

export default AppComponent;