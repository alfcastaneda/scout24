import './item.scss';
const ItemComponent = {
  bindings: {
    state: '<',
    isList: '<'
  },
  template: `
  <div class="state-item border padding shadow"
    ng-class="$ctrl.state.productType === 'L' ? 'sponsored' : ''" >
    <h3>{{$ctrl.state.title}}</h3>
    <p>{{$ctrl.state.addressToDisplay}} ({{$ctrl.state.floorSpace}})</p>
    <div ng-hide="$ctrl.isList" class="picture">
      <img ng-src="{{$ctrl.state.pictureUrl}}" />
      <span ng-class="[$ctrl.state.commercializationType === 'RENT' ? 'rent' : 'buy', 'tag']">
      {{$ctrl.state.commercializationType}}
      </span>
      <ul ng-hide="$ctrl.isList" class="list-check">
        <h4>Features</h4>
        <li ng-repeat="feat in $ctrl.state.features">{{feat}}</li>
      </ul>
    </div>
    <p ng-show="$ctrl.isList">Price fo area: {{$ctrl.state.priceForTotalArea}}</p>
  </div>
  `
};

export default ItemComponent;