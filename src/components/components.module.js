import angular from 'angular';
import ListViewComponent from './list/list.component';
import GroupViewComponent from './group/group.component';
import ItemComponent from './item/item.component';
import FeatureFilterComponent from './feature-filter/feature-filter.component';
import CommercializationFilterComponent from './commercialization-filter/commercialization.component';

const ComponentsModule = angular
  .module('demoApp.components', [])
  .component('listView', ListViewComponent)
  .component('groupView', GroupViewComponent)
  .component('item', ItemComponent)
  .component('featureFilter', FeatureFilterComponent)
  .component('commercializationFilter', CommercializationFilterComponent)
  .name;

export default ComponentsModule;