const CommercializationFilterComponent = {
  bindings: {
    updateStates: '&',
    defaultValue: '@'
  },
  template: `
    <span>Select Commercialization type</span>
    <select ng-model="$ctrl.cType" ng-change="$ctrl.changeType()">
      <option value="ALL" ng-selected>ALL</option>
      <option value="BUY">BUY</option>
      <option value="RENT">RENT</option>
    </select>
  `,
  controller: class CommercializationFilterComponent {
    $onInit() {
      this.cType = this.defaultValue;
    }

    changeType() {
      this.updateStates({value: this.cType});
    }
  }
}
export default CommercializationFilterComponent;