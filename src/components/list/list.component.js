import './list.scss';
import {copy} from 'angular';

const ListViewComponent = {
  template: `<item is-list="true" ng-repeat="state in $ctrl.states" state="state"></item>`,
  bindings: {
    realStates: '<'
  },
  controller: class ListViewComponent {
    constructor() {}
    $onInit() {}
    $onChanges({realStates}) {
      if (realStates && realStates.currentValue) {
        this.states = this.sortStates(realStates.currentValue)
      }
    }
    sortStates(states) {
      return copy(states).sort(s => {
        if (s.productType === 'L') {
          return -1;
        } else {
          return 1;
        }
      });
    }

  }
}

export default ListViewComponent;