import './group.scss';
const GroupViewComponent = {
  template: `
    <div ng-repeat="category in $ctrl.categories">
      <h3 class="category background">{{category}}***  <span style="float:right">({{$ctrl.classifiedStates[category].length}})</span></h3>
      <div class="items">
        <item is-list="false" ng-repeat="state in $ctrl.classifiedStates[category]" state="state">
        </item>
      </div>
    </div>
  `,
  bindings: {
    realStates: '<'
  },
  controller: class GroupViewComponent {
    constructor() {
      'ngInject';
    }

    $onChanges({realStates}) {
      if (realStates && realStates.currentValue) {
        this.classifiedStates = this.classifyStates(realStates.currentValue);
        this.categories = Object.keys(this.classifiedStates);
      }
    }

    classifyStates(states) {
      const zipCodes = states.reduce((acc, currentItem) => {
        const r = /^\d+$/;
        let postCode = currentItem.addressToDisplay
          .split(' ')
          .filter(probableZipCode => r.test(probableZipCode))
          .map(pc => pc.slice(0,2))
          .join(',');
        if (!acc[postCode]) {
          acc[postCode] = [];
        }
        acc[postCode].push(currentItem);
        return acc;
      }, {});
      return zipCodes;
    }
  }
}

export default GroupViewComponent;