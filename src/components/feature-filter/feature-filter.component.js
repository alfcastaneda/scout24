import './features.scss';
const FeatureFilterComponent = {
  bindings: {
    availableFeatures: '<',
    updateStates: '&'
  },
  template: `
    <div class="feature-filters" ng-form="$ctrl.featuresForm">
      <label ng-repeat="feature in $ctrl.features">
        <input type="checkbox" ng-model="feature.isChecked" ng-change="$ctrl.updateFilter()">
        {{feature.label | lowercase}}
      </label>
    </div>
  `,
  controller: class FeatureFilterComponent {
    constructor(){
      console.log('Creating a fucking filter');
    }

    $onChanges({availableFeatures}) {
      if (availableFeatures && availableFeatures.currentValue) {
        this.features = availableFeatures.currentValue.map(f => ({
          label: f.replace(/_/g, ' ').toLowerCase(),
          value: f,
          isChecked: false
        }));;
      }
    }

    updateFilter() {
      let selected = this.features
        .filter(f => f.isChecked)
        .map(f => f.value);
      this.updateStates({selected});
    }
  }
}

export default FeatureFilterComponent;
