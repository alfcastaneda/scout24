export default class HttpService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  getRealStates() {
    return this.$http.get('/entries')
      .then(response => response.data)
      .catch(e => {throw new Error(e)});
  }
}