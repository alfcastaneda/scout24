import angular from 'angular';
import AppComponent from './app.component'
import ComponentsModule from './components/components.module';
import HttpService from './services/http.service.js';

const demoApp = angular
  .module('demoApp', [
    ComponentsModule
  ])
  .service('HttpService', HttpService)
  .component('app', AppComponent)
  .name;

  angular.bootstrap(document, [demoApp]);

