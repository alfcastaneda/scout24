const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const fs = require('fs');

const app = express();
const configFn = require('./webpack.config.js');
const config = configFn();
const compiler = webpack(config);

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
// app.use(webpackDevMiddleware(compiler, {
//   publicPath: config.output.publicPath
// }));
app.use(require("webpack-dev-middleware")(compiler, {
  noInfo: true, publicPath: config.output.publicPath
}));
// Step 3: Attach the hot middleware to the compiler & the server
app.use(require("webpack-hot-middleware")(compiler, {
  log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
}));
app.get('/entries', function (req, res) {
  fs.readFile('serverResponse.txt', 'utf8', (err, data) => {
    if(err) throw err;
    const entries = data.split('\n').map(s => s.trim()).join('');
    const json =  JSON.parse(entries);
    const response = Object.assign({}, json, {meta: {total: json.resultlistEntries.length}})
    res.send(response);
  });
});

// Serve the files on port 3000.
app.listen(3000, function () {
  console.log('Example app listening on port 3000!\n');
});
