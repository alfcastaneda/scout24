# Scout24
Basic work for immobilien scout coding challenge

## Getting Started

As I personally couldn't assign enough time to this project I did it on Angularjs which is the framework I know the most.

### Installing

The setup is pretty simple

```
git clone https://bitbucket.org/alfcastaneda/scout24.git

cd scout24

npm install

npm run server
```



## Author

* **Alfredo Castañeda** - (https://github.com/hobbit1mx)
